package entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

import Values.Health;
import overlay.Hud;
import tools.PlayerContact;

/**
 * Abstract class, AbstractPlayer, which extends AbstractEntity. 
 * Contains all shared code between player-classes (as we need more than one player-class due to multiplayer functionality),
 * and code that is unique to the player-classes and should not be accessed by enemy-classes.
 */
public abstract class AbstractPlayer extends AbstractEntity {

	boolean canJump;
    PlayerContact cont;
    Texture left;
    Texture right;
    Texture jump;
    public Vector2 startPos;
	
    public boolean isDead = false;
	public static int health;
	public Health playerHealth;

	Sound jumpSound;
	
	public AbstractPlayer(Sprite sprite, TiledMapTileLayer collisionLayer) {
		super(sprite);
		speed = 150;
    	jumpSound = Gdx.audio.newSound(Gdx.files.internal("src/main/resources/sounds/jump.mp3"));
	}
	

	/**
	 * Checks if player is dead based on if player health is 0 or not.
	 * @return true if player health is 0
	 */
	public boolean isPlayerDead(){
		// || Hud.health.value == 0) for multiplayer. 
		if(getPlayerHealth() == 0 || Hud.health.value == 0) 
			isDead = true;
		else 
			isDead = false;
		return isDead;
	}

	/**
	 * Retrieves player health
	 * @return int - player health
	 */
	public int getPlayerHealth(){
		return playerHealth.getValue();
	}
	

	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);
		checkPlayerFall();
    }


	/**
	 * Checks if player fell of map where the player will take damage if done.
	 */
	private void checkPlayerFall() {
		if(getY() < -250){
			if(playerHealth.getValue() > 0){
				playerTakesDamage();
			}
		}
	}

	@Override
	public boolean checkCollisionX(boolean collisionX) {
		if(velocity.x < 0) // going left
          collisionX = cont.collidesLeft();
		else if(velocity.x > 0) // going right
          collisionX = cont.collidesRight();
		return collisionX;	
	}


	@Override
	public void reactCollisionX(float oldX, boolean collisionX) {
		if(collisionX) {
          setX(oldX);
          velocity.x = 0;
		}
	}


	@Override
	public boolean checkCollisionY(boolean collisionY) {
		if(velocity.y < 0) // going down
          canJump = collisionY = cont.collidesBottom();
		else if(velocity.y > 0) // going up
          collisionY = cont.collidesTop();
		return collisionY;
	}
	
	/**
	 * Method for player taking damage.
	 * Decrements player health, as well as decrements score. Updates HUD and places player on start position of level.
	 */
	public void playerTakesDamage() {
		playerHealth.valueDecrement(1);
		Hud.healthDecreaseHUD();
		Hud.scoreDecrementHUD(5);
		setPosition(startPos.x, startPos.y);
	}
	
}
