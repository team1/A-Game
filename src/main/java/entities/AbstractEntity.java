package entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

/**
 * Abstract class, AbstractEntity, which contains all shared code between entities (Players and Enemies).
 * Contains everything concerning entities, mostly physics and collision, as well as methods for rendering/drawing entity.
 */
public abstract class AbstractEntity extends Sprite {
	
	public Vector2 velocity = new Vector2();
	public float speed;
	public float gravity = 50 * 2f;
	public float deltaTime = Math.max(1 / 30f, Gdx.graphics.getDeltaTime());
	public String name;
	public float increment;
	public TiledMapTileLayer collisionLayer;
	
	public AbstractEntity(Sprite sprite) {
		super(sprite);
	}
	
	/**
	 * Get the name of the entity.
	 * @return string - the name of the entity
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Draws the entity on the map/world.
	 * @param spritebatch
	 */
	public void drawEntity(SpriteBatch spritebatch) {
		update(deltaTime);
		super.draw(spritebatch);		
	}
	
	/**
	 * Updates entity position, velocity and governs collision between objects and entity using contact-classes.
	 * @param deltaTime
	 */
	public void update(float deltaTime) {
		 // apply gravity
        applyGravity(deltaTime);

        // clamp y-velocity
        yVelocityClamper();

        // save old position
        float oldX = getX();
        float oldY = getY();
        
        //save collision states for x and y
        boolean collisionX = false;
        boolean collisionY = false;

        // move on x
        moveX(deltaTime);

        // calculate the increment for step in #collidesLeft() and #collidesRight()
        increment = collisionLayer.getTileWidth();
        calculateIncrement();

        //Checks for collision when moving in x direction.
        collisionX = checkCollisionX(collisionX);

        // react to collision in x direction
        reactCollisionX(oldX, collisionX);

        // move on y
        moveY(deltaTime);

        // calculate the increment for step in #collidesBottom() and #collidesTop()
        increment = collisionLayer.getTileHeight();
        calculateIncrement();

        collisionY = checkCollisionY(collisionY);

        // react to collision in y direction
        reactCollisionY(oldY, collisionY);
	}
	
	/**
	 * Moves entity in y-direction using setY().
	 * @param deltaTime
	 */
	public void moveY(float deltaTime) {
		setY(getY() + velocity.y * deltaTime * 5f);
		
	}
	
	/**
	 * Moves entity in x-direction using setX().
	 * @param deltaTime
	 */
	public void moveX(float deltaTime) {
		setX(getX() + velocity.x * deltaTime);		
	}
	
	/**
	 * Checks for collision in x-direction.
	 * @param collisionX
	 * @return boolean - true if there is collision in x-direction.
	 */
	abstract boolean checkCollisionX(boolean collisionX);
	
	/**
	 * Stops entity in x-direction using an oldX variable simulating collision when collisionX is true.
	 * @param oldX
	 * @param collisionX
	 */
	public void reactCollisionX(float oldX, boolean collisionX) {
		if(collisionX) {
			setX(oldX);
			velocity.x = 0;
		}
	}
	
	/**
	 * Checks for collision in y-direction.
	 * @param collisionY
	 * @return boolean - true if there is collision in y-direction.
	 */
	abstract boolean checkCollisionY(boolean collisionY);
	
	/**
	 * Stops entity in y-direction using an oldY variable simulating collision when collisionY is true.
	 * @param oldY
	 * @param collisionY
	 */
	public void reactCollisionY(float oldY, boolean collisionY) {
		if(collisionY) {
          setY(oldY);
          velocity.y = 0;
		}
	}
	
	/**
	 * Calculates increment variable based on tile width/height.
	 */
	public void calculateIncrement() {
		if(getWidth() < increment)
        	increment = getWidth() / 2;
		else
        	increment = increment / 2;
	}
	
	/**
	 * Clamps y-velocity, setting the cap of max speed in y-direction to arbitrary speed.
	 */
	public void yVelocityClamper() {
		if(velocity.y > speed)
          velocity.y = speed;
		else if(velocity.y < -speed)
          velocity.y = -speed;
	}
	
	/**
	 * Applies and simulates gravity on entity.
	 * @param deltaTime
	 */
	public void applyGravity(float deltaTime) {
		velocity.y -= gravity * deltaTime;		
	}

}
