package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;

import app.AGame;

/**
 * Main menu class which is the main menu that shows when executing Main.java, or when player presses ESCAPE/ESC.
 * Extends AbstractMenu.
 * Gives the option to start the game, either singleplayer or multiplayer, or quit the game.
 */
public class MainMenu extends AbstractMenu {
	
	private Music music;


	public MainMenu (AGame game) {
		super(game);
		
		stillPic = new Texture(Gdx.files.internal("src/main/resources/textures/aGame.png"));
		
		multiPlayer = new Texture(Gdx.files.internal("src/main/resources/textures/multi.png"));
		singlePlayer = new Texture(Gdx.files.internal("src/main/resources/textures/single.png"));
		singleInactive = new Texture(Gdx.files.internal("src/main/resources/textures/singleInactive.png"));
		multiInactive = new Texture(Gdx.files.internal("src/main/resources/textures/multiInactive.png"));
		
		button1Active = new Texture(Gdx.files.internal("src/main/resources/textures/start.png"));
		button1Inactive = new Texture(Gdx.files.internal("src/main/resources/textures/startInactive.png"));
		button2Active = new Texture(Gdx.files.internal("src/main/resources/textures/quit.png"));
		button2Inactive = new Texture(Gdx.files.internal("src/main/resources/textures/quitInactive.png"));
		
	}

	@Override
	public void show() {
		//Sets the counter to 1 when main menu is opened as to start from level 1.
		counter = 1;
		music = Gdx.audio.newMusic(Gdx.files.internal("src/main/resources/music/menuMusic.mp3"));
		music.setLooping(true);
		music.setVolume(0.5f);
		music.play();
	}
	
	
	@Override
	public void render(float delta) {
        super.render(delta);
		
        game.batch.begin();
        
        touchCheckQuit(button2Active, button2Inactive, button4Y, buttonX, game);
        
        if(touchCheckLevelChange(singlePlayer, singleInactive, button1Y, buttonX, game))
        	AbstractGameScreen.multiplayerBit = 0;
        
        if(touchCheckLevelChange(multiPlayer, multiInactive, button3Y, buttonX, game))
        	AbstractGameScreen.multiplayerBit = 1;
        
        game.batch.end();
        
		
	}
	
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		music.dispose();
	}

}
