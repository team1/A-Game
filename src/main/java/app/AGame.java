package app;

import com.badlogic.gdx.Game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import screens.MenuCamera;
import screens.MainMenu;

/**
 * AGame class
 * Game class which runs the game (executes the main menu).
 */
public class AGame extends Game {
	public static final int Width = 640;
	public static final int Height = 640;
	
	
    public SpriteBatch batch;
    public static MenuCamera menuCamera;
    

    @Override
    public void create() {
    	batch = new SpriteBatch();
    	menuCamera = new MenuCamera(Width, Height);
    	
    	this.setScreen(new MainMenu(this));
    	
    }

    @Override
    public void dispose(){
   
    }

    @Override
    public void render() {
    	batch.setProjectionMatrix(menuCamera.combined());
    	super.render();
    }

    @Override
    public void resize(int width, int height) {
    	menuCamera.update(width, height);
    	super.resize(width, height);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
}
