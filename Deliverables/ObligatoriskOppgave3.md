# Oblig 3 – A-Game

* Team: A-Team (Gruppe 4): Oscar Nysten, Audun Haugen, Marius Aasebø, Peder Spooren

## Deloppgave 1:

### Referat av teammøter:

#### 29.03.2022
Tilstede: Oscar, Audun, Marius, Peder.

I dette møtet startet vi tredje sprint, og tredje obligatorisk. Her diskuterte vi hva som skulle prioriteres, om noen endringer skulle bli gjort med prioriteringslisten, hva vi tenkte vi skulle prøve å få til frem til deadline og sånt. Vi ble enige om at av de resterende punktene i prioriteringslisten, så skal vi bare prøve å gjøre så mange av dem som mulig, istedenfor å begrense oss til ett fast punkt hvor vi sier stopp. Vi diskuterte videre de første punktene i prioriteringslisten og brainstormet litt om hvordan de skulle gjøres. Videre delte vi ut oppgaver til hver av oss som vi skulle jobbe med. Vi oppdaterte Trello basert på dette. Vi skrev da brukerhistorier på de aktuelle punktene og startet med å løse oppgavene vi delte ut. Til neste gang ble vi da enige om å fortsette med oppgavene vi delte ut. Vi ble også enige om neste møtetid.

#### 07.04.2022
Tilstede: Audun, Marius, Peder.

Grunnet sykdom og andre omstendigheter fikk ikke vi fikse noen flere møter mellom denne og den forrige. Vi har fortsatt fått gjort noe fram til dette møtet. I dette møtet satte vi oss ned for å finne ut det siste av poengsystemet og bevegelse og kollisjon av fiender. Vi fikset også enkel musikk og lydeffekter under dette møtet. Videre diskuterte vi bugs som har dukket opp (som man kan se under bugliste nederst i denne filen), og hva som skulle gjøres frem til deadline dagen etter. Vi ble enige om at dagen etter skulle vi fokusere på å gjøre oss ferdig med dokumentasjon (altså denne filen), diverse rydding og klargjøring av innlevering. 

#### 08.04.2022
Tilstede: Oscar, Marius, Peder.

Dette møtet var dedikert til dokumentasjon og rydding av kode, i tillegg til å ordne klassediagram, denne filen og diverse andre ting som skulle være med i leveransen til obligatorisk 3. Vi diskuterte rydding av kode, bugs, framtidig planer på enkelte ting (som f.eks. oppførsel mellom spiller og fiende, altså hvordan spiller skal "dø"/ta skade) og dokumentasjon. Uken etter møtet er det påskeferie, så vi kommer ikke i gang med neste sprint før etter påsken, men vi ble enige om at vi kan gjøre oppgaver som vi vil dersom man ønsker det, så lenge man sier i fra hva man jobber med. Til slutt i dette møtet, gjorde vi oss ferdig, ryddet opp det siste for leveransen til kunde. 

#### 10.04.2022
Tilstede: Marius, Peder.

Dette møtet var ment for å fullføre resterende arbeid som var ment for å være inkludert i dagens leveranse. Her gikk vi igjennom alle klasser og metoder for enkel rydding av kode, i tillegg til dokumentasjon av disse. Vi fikset også på ting basert på tilbakemeldingen vi fikk for forrige split. Her fikset vi også på Trello, som i samme slengen ble som en planlegging på hva vi skulle gjøre videre, så inn til neste split har vi allerede en plan klar. 


### Oppsummering (for punktene under referat)
Rollene i teamet har holdt seg likt som forrige obligatorisk, altså er rollene fortsatt slik:

Produktansvarlig / Grafikkansvarlig 	- Marius <br>
Physicsansvarlig / kvalitetskontroll	- Oscar <br>
Dokumentasjonsansvarlig / Scrum master  - Peder <br>
Git-expert / Kundekontakt 				- Audun <br>


Disse rollene fungerer fint, og vi velger derfor å beholde strukturen.

#### Rollenes betydning:
Her er rollenes betydning, som er lik de fra forrige obligatorisk.

- Produktansvarlig er personen som har ansvaret til å definere hvordan spillet skal være og se ut. Dette inkluderer også å gjennomgå brukerhistorier, i tillegg til å komme med diverse idéer.

- Grafikkansvarlig er ansvarlig for alt av det grafiske i spillet. Dette gjelder utseende av spillkarakter/spillbrett, menyer og diverse andre "skjermer" (loading screen, game-over screen).

- Physicsansvarlig er ansvarlig for alt om fysikk og movement av spillet og andre objekter i spillet. Dette inkluderer da tyngdekraft, kollisjon og alt av movement til spilleren. 

- Kvalitetskontroll er personen som forsikrer at koden ser ryddig og pen ut, i tillegg til at koden fungerer som den skal i spillet når man kjører det. Her kan man tenke at tester skal gjøre mye av jobben automatisk, men kvalitetskontrolløren skal overse leveransen og de manuelle testene. 

- Dokumentasjonsansvarlig er ansvarlig for alt av dokumentasjon i prosjektet. Denne personen skal ikke skrive dokumentasjonen til metodene/klassene andre personer skriver, men denne personen skal kontrollere om det ser pent, riktig og forståelig ut.

- Scrum master er personen som styrer teamet, alt fra å dirigere møter og holde prosjektet i gang. Denne personen er altså den som tar initiativ, og om det er noe uklart eller uenigheter, så er det scrum master som har veto.

- Git-experten skal ha kontroll over alt som skjer i git og repositoriet. Dette gjelder pushing, pulling, merging og leveransekontroll (sammen med kvalitetskontrollør) for å nevne noe. 

- Kundekontakt er den personen som har kontakt med kunde, enten om det er kunden som har problem, eller om det er vi som etterspør tilbakemelding. 


#### Prosjektmetodikk
Vi har fortsatt Scrum-metodikken som vi utviklet forrige obligatorisk. Dette opplever vi som velfungerende metodikk, som beskrevet i forrige obligatorisk. Vi har brukt Trello aktivt og har satt delmål under hvert "kort". Vi har holdt hverandre oppdatert over hva man har gjort, og korrigert ved behov. 

#### Gruppedynamikk
Gruppedynamikken, som sist, har fungert veldig bra. Vi har fortsatt ikke hatt noen store uenigheter, men av og til når vi diskuterer så kan det oppstå småe uenigheter, men disse blir fort løst. Kommunikasjonen flyter fint, og ingen har problemer med å snakke ut om de er uenige med noe.

#### Retrospektiv
Denne splitten har vært litt annerledes i forhold til de to forrige, da en del sykdom dukket opp, som forhindret oss i å møte fysisk. Til tross for dette, så jobbet vi med de oppgavene vi satte oss i det første møtet i splitten, og ble enige om å minimum fullføre disse. I de to siste møtene før deadline, gjorde vi de siste tingene som manglet for hver av oppgavene i fellesskap, i tillegg til enkel rydding av kode og dokumentasjon til obligatorisk. 

Kommunikasjon og push/pull/merging via git har gått fint, til tross for enkelte merge-conflicts, som fort ble fikset. 

Dessverre fikk vi ikke til å gjøre alle de MVP-kravene vi ville gjøre oss ferdig med til denne deadlinen, dette igjen grunnet sykdom. Heldigvis fikk vi gjort noen av kravene, slik at vi ikke føler det blir alt for mye arbeid i neste og siste splitt.

### Deloppgave 2:

### "Stretch goal"
"Stretch goal"-en er det samme som vi nevnte sist obligatorisk, altså vi bestemte oss for å i alle fall fikse multiplayer på samme masking, i tillegg til at vi tenker å prøve å fikse Android port om vi har tid til det.

### MVP og annet
#### Prioriteringsliste:
1. Fiender/monstre interagerer med terreng og spiller.
2. Spiller har poeng og interagerer med poenggjenstande
3. Spiller kan "dø"/tape (ved kontakt med fiender, eller ved å falle utfor skjermen)
4. Game-over skjerm
5. Restart-funksjon 
6. Musikk/sound effects
7. Mål for spillbrett (enten et sted, en mengde poeng, drepe alle fiender e.l.)
8. Flere nivåer/brett.
9. Nytt nivå når forrige er ferdig
10. Støtte flere spillere (enten på samme maskin eller over nettverk, stretch goal)
11. Android port (stretch goal 2)

Denne listen er tatt fra obligatorisk 2, hvor de punktene som vi allerede har gjort er fjernet.
Dette er da vår prioriteringsliste for denne splitten, hvor vi skal prøve å få til å implementere så mange som mulig fram til deadline. 


#### Brukerhistorier:
Her skriver vi brukerhistorier fortløpende som vi skal til å starte med en av punktene over:

- Som spiller må fiender interagere med terreng lik spilleren (kollisjon), i tillegg til å interagere med spiller og ha bevegelse, slik at jeg har en utfordring som vandrer på brettet.
	- Akseptansekriterier:
		- Fiender skal holde seg i bevegelse og ikke stå i ro.
		- Fiender skal bevege seg uavhengig av spiller.
		- Fiender skal ha lik interaksjon med terreng som spiller.
	- Arbeidsoppgaver:
		- Legge til bevegelse til fiende som er uavhengig av bevegelsen til spilleren.
		- Fikse kollisjon mellom fiende, terreng og spiller.
		
- Som spiller skal jeg kunne plukke opp poenggjenstander og få poeng slik at jeg få noe fremgang i spillet.
	- Akseptansekriterier:
		- Spiller skal plukke opp poenggjenstand og få noe poeng for det. Poengteller skal da oppdateres tilsvarende.
		- Poenggjenstand skal forsvinne fra brettet når spiller plukker det opp.
	- Arbeidsoppgaver:
		- Legge til poenggjenstand til spillet slik at de vises.
		- Fikse registrering av kontakt mellom spiller og poenggjenstand.
		- Implementere slik at når det forekommer kontakt, så skal poenggjenstanden "plukkes opp"/forsvinne fra brettet.
		- Implementere oppdatering av poengteller når poenggjenstand "plukkes opp". 

- Som spiller skal jeg kunne "dø"/tape slik at jeg kan tape spillet ved dårlig spilling.
	- Akseptansekriterier:
		- Spiller skal dø enten ved å ta for mye skade eller ved å falle ut av brettet.
	- Arbeidsoppgaver:
		- Implementere at spiller kan ta skade av fiender ved kontakt (og motsatt basert på hvilken type kontakt)
		- Spiller "dør"/taper ved 0 helse eller ved å falle utenfor brettet (faller for langt i y-retnign).
		- Spillet stopper når spiller taper.
		- Spillet gir en indikasjon av at spiller har tapt (spiller forsvinner og noe tekst som sier at spiller har "dødd"/tapt).
		
- Som spiller skal jeg kunne ha mulighet til å se en game-over skjerm hvor jeg kan velge hva jeg kan gjøre videre når jeg taper spillet, slik at jeg kan spille videre eller avslutte spillet.
	- Akseptansekriterier:
		- En game-over skjerm skal dukke opp når spiller taper spillet (helse <= 0).
		- Hver av knappene i skjermen skal gjøre det de beskriver (restart restarter spillet, main menu går til startskjermen, quit avslutter spiller o.l.).
	- Arbeidsoppgaver:
		- Implementere en ny skjerm som dukker opp når spiller taper som sier "Game Over".
		- Implementere knapper som "Restart, "Main Menu" og "Quit" som gjør det de beskriver.
		
- Som spiller skal jeg kunne høre musikk og lydeffekter slik at jeg får en bedre atmosfære av spillet og slik at jeg hører fiender og hva jeg gjør.
	- Akseptansekriterier:
		- Når spillet starter skal ny bakgrunnsmusikk spilles.
		- Lydeffektene skal dukke opp når de tilsvarende handlingene blir gjort (hopping, ta/gjør skade).
		- Bakgrunnsmusikken skal starte på nytt når spillet startes på nytt.
	- Arbeidsoppgaver:
		- Implementere bakgrunnsmusikk for startskjerm, spillskjerm og game-over skjerm. 
		- Implementere lydeffekter til forskjellige ting som hopping, enemies og det å ta/gjør skade. 
		
Av disse brukerhistoriene / kravene har vi fullført nr. 1, 2 og 3. Vi mangler fortsatt at spilleren kan "dø" / tape ved "feil" kontakt med fiende og game-over skjerm, som vi ikke rakk å fikse til denne deadlinen. 

Da fristen ble forflyttet ett par dager, rakk vi å legge til en Game over-skjerm og at spilleren kan tape ved å ta for mye liv. Man kan fortsatt ikke tape liv av å komme i kontakt med en fiende, men om man faller ned så "respawner" man og man taper ett liv. Når man har tapt nok liv til å bare ha 0 liv igjen (Spilleren har foreløpig 3 liv, så man må falle tre ganger får å tape), så kommer Game over-skjermen opp og spillet er ferdig. Her kan man da velge om å starte ett nytt spill eller å lukke spillet. 
		
### Buglist:
- Hvis man flytter seg i en retning og så plutselig flytter seg i annen/motsatt retning, hvor man fortsatt holder tasten for originale retning inn, og så slipper  tasten for den nye retningen, så stopper spilleren opp.
- Om man tar fullscreen/endrer størrelse på vinduet før man trykker start / i game over skjerm, blir størrelse av textures rare. 
- Sprites/texture plassering i forhold til object collision plassering er litt feil (scaling).
- Musikken i startmenyen ødelegger testene våres av en eller annen grunn ("Unable to allocate audio buffers"), og musikken er derfor ikke inkludert.

Fixed bugs:
- Spiller forsvinner ikke lengre når man flytter på spillvinduet.
- Spiller/fiende "hovrer" ikke lengre over bakken.
- Spiller kan ikke falle for alltid lenger, men man taper heller helse og blir "respawnet".

#### Oversikt over hva som har blitt fikset basert på tilbakemelding:
- Lagt til invite link til Trello i README.md.
- Assignet oppgaver til medlemmer i Trello slik at man ser hvem som gjør hva.
- Lagt til underoppgave i form av sjekkliste istedenfor i beskrivelse av hvert kort i Trello (med unntak av de kortene som allerede er under "Done").
- I alle fremtidige commit-meldinger skal det skrives om det har vært en felles økt. 
- Score ui element oppdateres basert på om spiller plukker opp poenggjenstand.
- Fiende beveger seg nå og kan "dø" ved kontakt med spiller.
- Spiller kan nå "dø"/tape (men kun ved å falle ut av kartet).
- Musikk i både startmeny og spillet gjør at testene våre krasjer. Disse er derfor ikke inkludert.

Vil også nevne her at stretch goal bestemte vi oss for å gjøre etter at vi har fått fikset det meste av spillet, slik at man først faktisk kan få spilt gjennom spillet (fra "start" til "slutt") alene før man legger til enda en spiller. Dette er fordi vi vil fokusere mer på å gjøre selve spillet så ferdig som mulig uten å måtte bruke mye tid på stretch goal og muligens levere ett mindre ferdig produkt. 
I tillegg vil vi nevne her at vi prøvde å finne ut hvordan man kan kjøre spillet fra terminal med javac Main.java / java Main.java. Dette har vi ikke funnet ut av enda så vi avventer denne funksjonaliteten. Grunnet dette må man da ha IDE for å kompilere og kjøre spillet. 
Angående testene som er avhengige av spiller input, har vi ikke funnet ut av, og dette har vi søkt rundt omkring i nettet for å prøve å løse men ikke funnet noen god løsning på. Her ville vi satt pris på om du kunne komme med noen tips.
Testverktøy som SpotBugs har vi ikke helt kommet til enda, da vi har prioritert å få fikset ett fungerende spill først og fremst. Derfor har det også ikke vært så mye fokus på testene. f

