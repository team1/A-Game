package tools;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import entities.AbstractEntity;

/**
 * Abstract class, AbstractContact, which contains all shared code between EnemyContact and PlayerContact.
 */
public abstract class AbstractContact {
	String blockedKey = "blocked";
	
	/**
	 * Checks if a cell (tile) in the game (x/y-position divided by tileWidth/tileHeight) contains the "blocked" key made in Tiled.
	 * Used for collision with terrain.
	 * @param x-coordinate
	 * @param y-coordinate
	 * @param entity - enemy/player
	 * @return true - if the cell/tile contains the "blocked" key.
	 */
	public boolean isCellBlocked(float x, float y, AbstractEntity entity) {
		TiledMapTileLayer.Cell cell = entity.collisionLayer.getCell((int) (x / entity.collisionLayer.getTileWidth()), (int) (y / entity.collisionLayer.getTileHeight()));
		return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey(blockedKey);
	}
	
	/**
	 * Check if cell to the right has keyword blocked or any other keyword based on entity.
	 * @return
	 */
	abstract boolean collidesRight();
	
	/**
	 * Check if cell to the left has keyword blocked or any other keyword based on entity.
	 * @return
	 */
	abstract boolean collidesLeft();
	
	/**
	 * Check if cell to the top has keyword blocked or any other keyword based on entity.
	 * @return
	 */
	abstract boolean collidesTop();
	
	/**
	 * Check if cell to the bottom has keyword blocked or any other keyword based on entity.
	 * @return
	 */
	abstract boolean collidesBottom();

}
