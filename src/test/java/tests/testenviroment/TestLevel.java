package tests.testenviroment;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import entities.Enemy;
import entities.Player1;
import entities.Player2;
import overlay.Hud;
import screens.AbstractGameScreen;
import tests.testenviroment.TestGame;

import java.util.ArrayList;

public class TestLevel  extends AbstractGameScreen {
    public TestGame testGame;
    public Player1 player;

    public TestLevel(TestGame testGame) {
        this.testGame = testGame;

        map = new TmxMapLoader().load("src/main/resources/levels/Level1.tmx");
        renderer = new OrthogonalTiledMapRenderer(map);

        //Makes both player objects in case of multiplayer, works even if player2 isn't used further (in singleplayer).
        player1 = new Player1(new Sprite(new Texture("src/main/resources/textures/player1Right.png")) , (TiledMapTileLayer) map.getLayers().get(0));
        player2 = new Player2(new Sprite(new Texture("src/main/resources/textures/player2Right.png")) , (TiledMapTileLayer) map.getLayers().get(0));

        for(int i = 0; i < 4; i++) {
            enemies.add(new Enemy(new Sprite(new Texture("src/main/resources/textures/spider.png")), (TiledMapTileLayer) map.getLayers().get(0)));
        }

        hud = new Hud(testGame.batch);

        score = Hud.score;

    }

    @Override
    public void show() {
        super.show();
        player1.setPosition(64, 100);
        enemies.get(0).setPosition(320, 192);
        enemies.get(1).setPosition(1216, 64);
        enemies.get(2).setPosition(1728, 64);
        enemies.get(3).setPosition(3264, 192);

    }
}
