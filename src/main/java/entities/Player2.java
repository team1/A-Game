package entities;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

import Values.Health;
import tools.PlayerContact;

/**
 * Player2-class, which extends AbstractPlayer and implements InputProcessor.
 * Contains code for player2, though is almost identical to Player1-class, with exception to inputs and position.
 */
public class Player2 extends AbstractPlayer implements InputProcessor {

	public Player2(Sprite sprite, TiledMapTileLayer collisionLayer) {
		super(sprite, collisionLayer);
		name = "A-Player2";
		health = 3;
		playerHealth = new Health(health);
		startPos = new Vector2(128, 200);
		this.collisionLayer = collisionLayer;
		cont = new PlayerContact(this);
		this.left = new Texture(Gdx.files.internal("src/main/resources/textures/player2Left.png"));
    	this.right = new Texture(Gdx.files.internal("src/main/resources/textures/player2Right.png"));
    	this.jump = new Texture(Gdx.files.internal("src/main/resources/textures/player2Jump.png"));
	}

	@Override
	public boolean keyDown(int keycode) {
		switch(keycode) {
		case Keys.UP:
			if(canJump) {
				setTexture(jump);
				velocity.y = speed / 1.8f;
				canJump = false;
				jumpSound.play(0.05f);
			}
			break;
		
		case Keys.LEFT:
			setTexture(left);
			velocity.x = -speed;
			break;
				
		case Keys.RIGHT:
			setTexture(right);
			velocity.x = speed;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode) {
		case Keys.UP:
			setTexture(right);
			break;
		case Keys.LEFT:
		case Keys.RIGHT:
			velocity.x = 0;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(float amountX, float amountY) {
		// TODO Auto-generated method stub
		return false;
	}

}
