<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.1" name="EmptyTile_64x64" tilewidth="64" tileheight="64" tilecount="1" columns="1">
 <image source="EmptyTile_64x64.png" width="64" height="64"/>
 <tile id="0">
  <properties>
   <property name="enemyBlocked" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
