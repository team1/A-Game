package Values;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

/**
 * Score class extends AbstractValue.
 * Contains the methods of AbstractValue with additional sound effects added.
 */
public class Score extends AbstractValue {
//	private int score;
	
	public static final int rubyValue = 5;
	
	private Sound pointIncrement;
	private Sound pointDecrement;
	
	public Score(int score) {
		value = score;
		pointIncrement = Gdx.audio.newSound(Gdx.files.internal("src/main/resources/sounds/pointincrement.mp3"));
		pointDecrement = Gdx.audio.newSound(Gdx.files.internal("src/main/resources/sounds/hurt.mp3"));
		
	}
	
	@Override
	public void valueIncrement(int value) {
		super.valueIncrement(value);
		pointIncrement.play(0.1f);
	}

	@Override
	public void valueDecrement(int value) {
		super.valueDecrement(value);
		pointDecrement.play(0.1f);
	}

}
