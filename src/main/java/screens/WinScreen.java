package screens;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;

import app.AGame;

/**
 * WinScreen-class which shows a win-screen once the player has obtained the win condition given.
 * Extends AbstractMenu.
 * Gives the option to go to the next or previous level.
 */
public class WinScreen extends AbstractMenu {
	
	private Music winScreenMusic;
	
	public WinScreen (AGame game) {
		super(game);
		
		stillPic = new Texture(Gdx.files.internal("src/main/resources/textures/levelComplete.png"));
		button1Active = new Texture(Gdx.files.internal("src/main/resources/textures/continue.png"));
		button1Inactive = new Texture(Gdx.files.internal("src/main/resources/textures/continueInactive.png"));
		button2Active = new Texture(Gdx.files.internal("src/main/resources/textures/previous.png"));
		button2Inactive = new Texture(Gdx.files.internal("src/main/resources/textures/previousInactive.png"));
		
	}

	@Override
	public void show() {
		winScreenMusic = Gdx.audio.newMusic(Gdx.files.internal("src/main/resources/music/winScreenMusic.mp3"));
		winScreenMusic.setLooping(true);
		winScreenMusic.setVolume(0.5f);
		winScreenMusic.play();
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);

		game.batch.begin();

		touchCheckLevelChange(button1Active, button1Inactive, button2Y, buttonX, game);
        
        touchCheckLevelChange(button2Active, button2Inactive, button4Y, buttonX, game);
        
        game.batch.end();
		
	}

	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		winScreenMusic.dispose();
	}

}