package screens.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import app.AGame;
import entities.Enemy;
import entities.Player1;
import entities.Player2;
import overlay.Hud;
import screens.AbstractGameScreen;

/**
 * GameScreen-class for level2, extends AbstractGameScreen.
 * Holds all code unique or necessary for level2.
 */
public class level2 extends AbstractGameScreen {
	
	public level2(AGame game) {
        this.game = game;
        
        map = new TmxMapLoader().load("src/main/resources/levels/Level2.tmx");
    	renderer = new OrthogonalTiledMapRenderer(map);
    	
    	//Makes both player objects in case of multiplayer, works even if player2 isn't used further (in singleplayer).
    	player1 = new Player1(new Sprite(new Texture(Gdx.files.internal("src/main/resources/textures/player1Right.png"))) , (TiledMapTileLayer) map.getLayers().get(0));
    	player2 = new Player2(new Sprite(new Texture(Gdx.files.internal("src/main/resources/textures/player2Right.png"))) , (TiledMapTileLayer) map.getLayers().get(0));

        for(int i = 0; i < 6; i++) {
        	enemies.add(new Enemy(new Sprite(new Texture(Gdx.files.internal("src/main/resources/textures/spider.png"))), (TiledMapTileLayer) map.getLayers().get(0)));
        }
    	
        hud = new Hud(game.batch);
        
        score = Hud.score;
        
    }
	
	@Override
	public void show() {
		super.show();
        enemies.get(0).setPosition(320, 192);
        enemies.get(1).setPosition(1216, 64);
        enemies.get(2).setPosition(1600, 64);
        enemies.get(3).setPosition(2112, 64);
        enemies.get(4).setPosition(3200, 192);
        enemies.get(5).setPosition(2368, 512);
        
	}


}
