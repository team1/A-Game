package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import app.AGame;

/**
 * A Menu-Camera class which governs the scaling of screen and buttons in all of the menu-screens.
 */
public class MenuCamera {
	
	OrthographicCamera menuCamera;
	StretchViewport viewport;
	
	public MenuCamera(int width, int height) {
		menuCamera = new OrthographicCamera();
		viewport = new StretchViewport(AGame.Width, AGame.Height, menuCamera);
		viewport.apply();
		menuCamera.position.set(AGame.Width / 2, AGame.Height / 2, 0);
		menuCamera.update();
		
	}
	
	/**
	 * A method that returns menuCamera.combined, used for setProjectionMatrix-method.
	 * @return
	 */
	public Matrix4 combined() {
		return menuCamera.combined;
	}
	
	/**
	 * update method used to update the viewport.
	 * @param width
	 * @param height
	 */
	public void update(int width, int height) {
		viewport.update(width, height);
	}
	
	/**
	 * Method used for scaling of screen and buttons.
	 */
	public Vector2 getInputInGameWorld() {
		Vector3 inputScreen = new Vector3(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY(), 0);
		Vector3 unprojected = menuCamera.unproject(inputScreen);
		return new Vector2(unprojected.x, unprojected.y);
	}

}
