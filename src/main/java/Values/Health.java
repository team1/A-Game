package Values;

/**
 * Health class, extends AbstractValue.
 */
public class Health extends AbstractValue {
	
	public Health(int health) {
		value = health;
	}

}
