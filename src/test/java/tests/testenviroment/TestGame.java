package tests.testenviroment;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TestGame extends Game {
    public static final int V_Width = 640;
    public static final int V_Height = 640;

    public SpriteBatch batch;

    @Override
    public void create() {
        this.setScreen(new TestLevel(this));

        batch = new SpriteBatch();
        Gdx.app.exit();
    }

    @Override
    public void dispose(){

    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
}
