package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;

import app.AGame;

/**
 * A final GameCompleteScreen-class which is used as the win-screen after completing the last level in the game.
 * Extends AbstractMenu.
 * Gives the player the option to quit or go back to main menu.
 */
public class GameCompleteScreen extends AbstractMenu {

	private Music winScreenMusic;

	public GameCompleteScreen(AGame game) {
		super(game);
		
		stillPic = new Texture(Gdx.files.internal("src/main/resources/textures/gameComplete.png"));
		button1Active = new Texture(Gdx.files.internal("src/main/resources/textures/mainMenu.png"));
		button1Inactive = new Texture(Gdx.files.internal("src/main/resources/textures/mainMenuInactive.png"));
		button2Active = new Texture(Gdx.files.internal("src/main/resources/textures/quit.png"));
		button2Inactive = new Texture(Gdx.files.internal("src/main/resources/textures/quitInactive.png"));
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
		
		super.render(delta);

		game.batch.begin();

		touchCheckQuit(button2Active, button2Inactive, button4Y, buttonX, game);
        
		touchCheckMainMenu(button1Active, button1Inactive, button2Y, buttonX, game);
        
        game.batch.end();
	}
	
	@Override
	public void show() {
		winScreenMusic = Gdx.audio.newMusic(Gdx.files.internal("src/main/resources/music/ultimateWinMusic.mp3"));
		winScreenMusic.setLooping(true);
		winScreenMusic.setVolume(0.5f);
		winScreenMusic.play();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		
	}

}
