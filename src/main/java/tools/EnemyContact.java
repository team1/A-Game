package tools;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import entities.AbstractEntity;
import entities.Enemy;

/**
 * EnemyContact class, extends AbstractContact.
 * Contains methods governing contact/collision between enemies and other objects.
 */
public class EnemyContact extends AbstractContact {
    private Enemy enemy;
    
    // Unique block key for enemy for transparent tile in Tiled. Used for holding enemies on platforms.
    String enemyBlockedKey = "enemyBlocked";

    public EnemyContact(Enemy enemy) {
    	this.enemy = enemy;
	}

    //Added functionality for transparent tile in Tiled.
	@Override
	public boolean isCellBlocked(float x, float y, AbstractEntity enemy) {
        TiledMapTileLayer.Cell cell = enemy.collisionLayer.getCell((int) (x / enemy.collisionLayer.getTileWidth()), (int) (y / enemy.collisionLayer.getTileHeight()));
        return cell != null && cell.getTile() != null && (cell.getTile().getProperties().containsKey(blockedKey) || cell.getTile().getProperties().containsKey(enemyBlockedKey));
    }

	@Override
	public boolean collidesRight() {
        for(float step = 0; step <= enemy.getHeight(); step += enemy.increment)
            if(isCellBlocked(enemy.getX() + enemy.getWidth(), enemy.getY() + step, enemy))
                return true;
        
        return false;
    }

	@Override
	public boolean collidesLeft() {
        for(float step = 0; step <= enemy.getHeight(); step += enemy.increment)
            if(isCellBlocked(enemy.getX(), enemy.getY() + step, enemy))
                return true;
        
        return false;
    }

	@Override
	public boolean collidesTop() {
        for(float step = 0; step <= enemy.getWidth(); step += enemy.increment)
            if(isCellBlocked(enemy.getX() + step, enemy.getY() + enemy.getHeight(), enemy))
                return true;

        return false;
    }

	@Override
	public boolean collidesBottom() {
        for(float step = 0; step <= enemy.getWidth(); step += enemy.increment)
            if(isCellBlocked(enemy.getX() + step, enemy.getY(), enemy))
                return true;
        
        return false;
    }

}
