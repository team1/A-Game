# INF112 Group Project 
## Gruppe 4 - Team 1

### Team
Product manager / Graphics manager 	        - Marius Aasebø <br>
Game mechanics manager / Quality controller	- Oscar Nysten<br>
Documentation manager / Scrum master        - Peder Spooren<br>
Git-expert / Customer contact 				- Audun Haugen<br>


### Description
This project is a schoolproject where we make a super mario-like game. In our case, we're making the game, A-Game. A simple platform game.

The goal with this project is to learn how to work in teams, using project methodology which is what is being done in modern software development. This is supposed to give us a insight on work environments in software development. 

Current functions:
- Simple physics system, including gravity and collision with terrain.
- Movement of player
- Collision and interaction between player and enemies.
- Currently three working levels.

The project is set up with Maven, libGdx and Tiled is being used as map editor.

### Requirements
Java 8+ <br>
Version Control, git etc. <br>
Maven <br>

### Clone and run using IDE
1. Install a Java IDE, such as Eclipse, open it. 
2. Before cloning you need to set up an SSH-key by following this guide: https://docs.gitlab.com/ee/user/ssh.html
3. Now with the SSH-key set up, go to the repository and click clone and copy the link associated with SSH.
4. Then, in your IDE, go to File -> Import -> Import from Git (with smart import) -> Clone URI.
5. Copy paste the SSH-link in the URI filed nad press Next.
6. Press Next once it is done loading, and then Next again, and then Finish.
7. Now the project should've been successfully cloned, to run the game, right-click Main.java, select Run as Java application.

### Clone and run using commandline
1. **Download Maven from here:** https://maven.apache.org/download.cgi
    
    Here's a quick video guide on how to install it properly: https://www.youtube.com/watch?v=qPkrvIGUvtU
2. **Clone the project using SSH/HTTPS:**
    ```
    SSH: git clone git@git.app.uib.no:team1/A-Game.git
    HTTPS: git clone https://git.app.uib.no/team1/A-Game.git
    ```
3. **Build the project**

    Unix-based systems:
    ```
    cd .../A-Game
    mvn clean package
    ```

    Windows:
    ```
    cd C:\...\A-Game
    mvn clean package
    ```
4. **Run the project**

    Linux:
    ```
    cd .../A-Game
    java -jar target/a-game-1.0-SNAPSHOT-fat.jar
    ```

    macOS:
    ```
    cd .../A-Game
    java -XstartOnFirstThread -jar target/a-game-1.0-SNAPSHOT-fat.jar
    ```

    Windows:
    ```
    cd C:\...\A-Game
    java -jar target/a-game-1.0-SNAPSHOT-fat.jar
    ```

### Trello
Trello invite: https://trello.com/invite/theateam56792781/384e07271977110340c15ce9b62c8ab0

### Currently known bugs:
- If you move in one direction and then suddenly move in another, where you still hold down the key for the original direction, and then let go of the key in the new direction, the player will stop (though this seems to be due to how the movement methods works for player).
- Sometimes the win condition get ignored, and therefore, the player can sometimes get a score above it (win condition is currently at 70 points) without being able to continue to the next level. An easy fix for this is to restart the game.
- HUD scaling is wrong when changing screen size before going into a new level.

### Licence
Player/Enemy and Contact classes are based on dermetfans work: <br>
Copyright (C) dermetfan, All rights reserved
URL: https://hg.sr.ht/~dermetfan/tiledmapgame/browse/TiledMapGame/src/net/dermetfan/tiledMapGame/entities/Player.java?rev=tip

Other sources:
- menuMusic: https://www.chosic.com/download-audio/29282/
- winScreenMusic: https://www.chosic.com/download-audio/29786/
- gameMusic: https://www.chosic.com/download-audio/31992/
- hurtSound: http://freesoundeffect.net/sound/cartoon-minion-hurt-sound-effect
- last win music: https://www.videvo.net/royalty-free-music-track/ultimate-victory-30s/232836/
