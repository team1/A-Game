package tests;

import static org.junit.jupiter.api.Assertions.*;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import entities.*;
import org.junit.jupiter.api.*;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

import app.AGame;
import screens.MainMenu;
import tests.testenviroment.TestGame;


public class AppTest {
	
	static TestGame game = new TestGame();
	static AGame agame = new AGame();
	private Player1 p1;
	private Enemy enemy;
	public MainMenu mm;

	/**
	 * Static method run before everything else
	 */
	@BeforeAll
	static void setUpBeforeAll() {
		Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
		cfg.setWindowedMode(TestGame.V_Width, TestGame.V_Height);
		new Lwjgl3Application(game, cfg);

	}

	@BeforeEach
	void setUp(){
		TiledMap map = new TmxMapLoader().load("src/main/resources/levels/Level1.tmx");
		p1 = new Player1(new Sprite(new Texture("src/main/resources/textures/player1Right.png")), (TiledMapTileLayer) map.getLayers().get(0));
		enemy = new Enemy(new Sprite(new Texture("src/main/resources/textures/spider.png")), (TiledMapTileLayer) map.getLayers().get(0));
		mm = new MainMenu(agame);
	}

	@Test
	void testPlayerOneSprite(){
		assertEquals(new Texture("src/main/resources/textures/player1Right.png").toString(), p1.getTexture().toString());
	}

	@Test
	void testSpiderTexture(){
		assertEquals(new Texture("src/main/resources/textures/spider.png").toString(), enemy.getTexture().toString());
	}

	@Test
	void testGameSize(){
		assertEquals(640, TestGame.V_Width);
		assertEquals(640, TestGame.V_Height);
	}
}