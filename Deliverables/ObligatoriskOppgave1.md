# Oblig 1 – A-Game

* Team: A-Team (Gruppe 4): Oscar Nysten, Audun Haugen, Marius Aasebø, Peder Spooren

## Deloppgave 1:

### Erfaring

Vi har lik erfaring siden alle går på datateknologi i samme kull.

### Roller

Teamlead		 	     - Marius<br>
Grafikkansvarlig	     - Oscar<br>
Testansvarlig		     - Peder<br>
Implementasjonsansvarlig - Audun<br>


Siden vi hadde lik erfaring, så hadde ikke valget av roller noe særlig å si for oss. 
Vi spurte rundt om det var noen som foretrukket en spesifikk rolle, hvis ikke ble det valgt tilfeldig.

Etterhvert som vi jobber videre med prosjektet kan vi tilpasse roller.
Vi kjenner ikke hverandre fra før, og det vil nok være mer naturlig etterhvert.


## Deloppgave 2:

Vi valgte kanban-metodikken, da vi planlegger omfattende bruk av Trello.
Teamet likte kanban på grunn av enkelt oppsett, få regler og fleksibel arbeidsmettodikk.

- Garantert fysiske møter på tirsdager (gruppetimer)
- Fleksible møter/avtale av tid. <br>
- Tildeler oppgaver når vi etterhvert finner ut hva oppgaver er. Fleksibel tildeling.<br>
- Oppfølger arbeid gjennom Trello/samtaler om det i møter.<br>
- Begynner med å skape idéer, for så å organsiere, og så videre starte med kode/testing.<br>
- Starter prosjektet med planlegging og diskusjon. Om vi sitter fast på noe alle sammen, er det lurt med en revurdering (reset-switch).
- Ved deling av oppgaver og kode bruker vi GIT og Trello.<br>



## Deloppgave 3:

- Ett plattform spill hvor man kan hoppe, eliminere fiender, mulighet for å stå på plattformer.

- Brukerhistorier:
	 - Som spiller trenger jeg en start meny for å kunne navigere meg til å starte spillet.
        - Meny for navigasjon.
        - Her må vi lage en GUI.
        
    - Som spiller trenger jeg fast grunn å stå på for å ikke falle ned.
        - Bunngrunn som man kan stå på.
        - Her må vi lage en plattform ved hjelp av GDX og tiled.
        
    - Som spiller trenger jeg å kunne bevege meg for å avansere i spillet. 
        - Mulighet for horisontal bevegelse.
        - Her må vi lage en move klasse som styrer bevegelse av karakter.
        
    - Som spiller trenger jeg å kunne hoppe for å avansere i spillet.
        - Mulighet for vertikal bevegelse.
        - Videre utvikle move klassen slik at man kan hoppe.
        
    - Som spiller trenger jeg plattformer som jeg kan hoppe fra og til.
        - Plattformer som kan hoppes fra og til.
        - Videre utvikle plattform konseptet.
        
    - Som spiller trenger jeg utfordringer i form av fiender for å utfordre meg selv.
        - Fiender som skaper utfordringer.
        - Implementere fiende klasse som kan gjøre "skade" på spiller.
        
    - Som spiller trenger jeg kunne forsvare meg (hoppe på fiender for å elimenere) for å overleve.
        - Mulighet for å eliminere fiender.
        - Implementere angrep eller noe lignede som bygger på move.
        
    - Som spiller trenger jeg å kunne bli eliminert for å slutte spillet.
        - Mulighet for å bli eliminert av fiender.
        - Implemente "game over" skjerm, som dukker opp ved helse == 0.
        
    - Som spiller trenger jeg å kunne samle poeng for å fullføre spillet.
        - Mulighet for å få poeng.
        - Legge til gjenstander som gir poeng, "counter" for poeng, med krav for å kunne fullføre spillet.
        
     - Som spiller trenger jeg å kunne samle objekter i spillet som forsterker spilleren.
        - Mulighet for å få plukke opp gjenstader.
        - Legge til mulighet for å plukke opp gjenstander, som gir mer helse, skade osv.    
            
    - Som spiller trenger jeg å kunne spille med andre spillere for å føle samhold.
        - Mulighet for å få venner.    
        - Implementere "multiplayer" for å kunne spille flere spillere samtidig, "multiplayer" klasse.
        
    - Som spiller trenger jeg ett mål for å fullføre spillet.
        - Mulighet for å bli ferdig. 
        - Implementere "win condition" som slutter spillet.
        
    - Som programør trenger jeg å kunne gjøre de samme tingene en spiller kan for å teste produktet ved hvert steg.
        - Mulighet for å utvikle tester.
        - Implementere tester.
        
    - Som programør trenger jeg å vite når spiller er ferdig slik at jeg kan test om spillet terminerer når målet er oppnåd.
        - Mulighet for å utvikle tester for spill slutt.    
        - Implementere tester.
        

- Prioritert liste:
    - Vise et spillebrett
    - Vise spiller på spillebrett
    - Flytte spiller (vha taster e.l.)


## Deloppgave 5:

## Oppsumering:

Vi har jobbet ca. 15 timer i løpet av denne obligatoriske oppgaven, dette har vi gjort i fellesskap.
Vi har samlet oss 6 ganger, ved bruk av grupperom, gruppetimer og ved VilVite senteret.
Grunnen til at vi har valgt å møtes fysisk og jobbe sammen i steden for hver for oss. Er at vi ville bygge ett skikkelig fundament
for dette prosjektet. Dette gjelder også roller for prosjektet, da dette er vanskelig å definere uten å ha jobbet sammen.

I løpet av start fasen har vi laget MVP 1-3, startet på implementasjon av tester, skrevet brukerhistorie/dokumentasjon.
Vi har også brukt mye tid på oppsett i GIT og funnet ut av hvordan vi bruker oppsett og maven struktur med merging osv.

Vi har brukt Trello til å planlegge etter kanban struktur.

For videre arbeid med prosjketet har vi satt opp fast møtetid fredager fra 10-14.


Bra:
- Sammarbeid
- Bruk av nettresurser (tutorials)
- Trello


Dårlig:
- Tester har vi ikke funnet helt ut av enda (challenge accepted)
- GIT issues
- Merge conflicts


Selv vurdering:

-Vi er veldig fornøyd med egen innsats. 
-Vi har lagt mye tid ned i prosjektet allerede og føler vi har skapt ett godt fundament for framtiden.

### Idéer

- Ekstra ting vi har diskutert:
	 - Salto (enten double jump = salto eller random chance)
	 - flere forskjellige karakterer med forskjellige angrep/evner (smash-lignende) 
	 - musikk/sfx


	- Vertikal jumper-spill (om å komme så høyt som mulig uten å dø)<br>
	- Smash bros/Tekken-lignende spill (multiplayer basert, forskjellige karakterer med forskjellige angrep/evner)<br>
	- Normal mario/pokémon-lignende spill (RPG - storybasert)<br>
	- Flappy-bird lignende spill


